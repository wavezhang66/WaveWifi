/*****************************************************************************
*  Copyright (C) 2019 Wave.Zhang  wavezhang@163.com.                         *
*                                                                            *
*                                                                            *
*  @file     WeWifi.h                                                       *
*  @brief    Wifi访问控制的功能                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2019/07/01 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/


#pragma once
#include <string>
#include<windows.h>
#include<wlanapi.h>
using namespace std;
class WeWifi
{
public:
	WeWifi();
	~WeWifi();

public:
	int listenStatus();
    int chekWifiResult(std::string wifiSsid);
	int decryptWifi(std::string wifiSsid, int start_line_number);
	void startDecryptWifi(std::string wifiSsid, int start_line_number);
	LPCWSTR StringToLPCWSTR(std::string orig);
	bool SetProfile(HANDLE hClient, std::string targetKey, PWLAN_INTERFACE_INFO, PWLAN_AVAILABLE_NETWORK pNet,
	                LPCWSTR pstrProfileXml);
	LPCWSTR getXmlProfile(HANDLE hClient
	                      , std::string targetKey
	                      , PWLAN_INTERFACE_INFO pIfInfo
	                      , PWLAN_AVAILABLE_NETWORK pNet
	                      , LPCWSTR pstrProfileXml);
};
