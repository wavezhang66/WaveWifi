#include "pch.h"
#include "WeNetWork.h"

#pragma comment(lib, "WS2_32.lib")
using namespace std;

WeNetWork::WeNetWork()
{
}

WeNetWork::~WeNetWork()
{
}

int WeNetWork::is_Get_Local_Ip()
{
	WSADATA wsData;
	::WSAStartup(MAKEWORD(2, 2), &wsData);
	char* ipAddressValue;
	char host[100] = {0};
	int reValue;
	string ipValue;
	reValue = 0;
	if (gethostname(host, sizeof(host)) < 0)
	{
		return -1;
	}

	struct hostent* hp;
	if ((hp = gethostbyname(host)) == NULL)
	{
		return -1;
	}

	int i = 0;
	while (hp->h_addr_list[i] != NULL)
	{
		printf("hostname: %s\n", hp->h_name);
		ipAddressValue = inet_ntoa(*(struct in_addr*)hp->h_addr_list[i]);

		printf("    ip:%s\n", ipAddressValue);
		ipValue = ipAddressValue;

		if (ipValue.compare("127.0.0.1") == 0)
		{
			reValue = -1;
		}

		i++;

		::WSACleanup();
		return reValue;
	}
}
