#include "pch.h"
#include "WeString.h"

#include "WeString.h"
#include "pch.h"
#include <string>
using namespace std;
WeString::WeString()
{
}

WeString::~WeString()
{
}
string WeString::substring(string ch, int pos, int length)
{
    //定义字符指针 指向传递进来的ch地址
    string* pch = &ch;
    //通过calloc来分配一个length长度的字符数组，返回的是字符指针。
    string* subch = (string*)calloc(sizeof(string), length + 1);
    int i;
    //只有在C99下for循环中才可以声明变量，这里写在外面，提高兼容性。
    pch = pch + pos;
    //是pch指针指向pos位置。
    for (i = 0; i < length; i++) {
        subch[i] = *(pch++);
        //循环遍历赋值数组。
    }

    subch[length] = '\0'; //加上字符串结束符。
    return *subch; //返回分配的字符
}
wchar_t* MBCS2Unicode(wchar_t* buff, const char* str)
{
    wchar_t* wp = buff;
    char* p = (char*)str;
    while (*p) {
        if (*p & 0x80) {
            *wp = *(wchar_t*)p;
            p++;
        } else {
            *wp = (wchar_t)*p;
        }
        wp++;
        p++;
    }
    *wp = 0x0000;
    return buff;
}

char* Unicode2MBCS(char* buff, const wchar_t* str)
{
    wchar_t* wp = (wchar_t*)str;
    char *p = buff, *tmp;
    while (*wp) {
        tmp = (char*)wp;
        if (*wp & 0xFF00) {
            *p = *tmp;
            p++;
            tmp++;
            *p = *tmp;
            p++;
        } else {
            *p = *tmp;
            p++;
        }
        wp++;
    }
    *p = 0x00;
    return buff;
}

wstring str2wstr(string str)
{
    size_t len = str.size();
    wchar_t* b = (wchar_t*)malloc((len + 1) * sizeof(wchar_t));
    MBCS2Unicode(b, str.c_str());
    wstring r(b);
    free(b);
    return r;
}

int wputs(wstring wstr)
{
    wputs(wstr.c_str());
    return 0;
}

int wputs(const wchar_t* wstr)
{
    int len = wcslen(wstr);
    char* buff = (char*)malloc((len * 2 + 1) * sizeof(char));
    Unicode2MBCS(buff, wstr);
    printf("%s", buff);
    free(buff);
    return 0;
}

string wstr2str(wstring wstr)
{
    size_t len = wstr.size();
    char* b = (char*)malloc((2 * len + 1) * sizeof(char));
    Unicode2MBCS(b, wstr.c_str());
    string r(b);
    free(b);
    return r;
}
void genPassword()
{
    string str = "abcdefghijklmnopqrstuvwxyz";
    string currentStrng;
    ofstream f1("E:\\software\\password\\pwd.txt"); //打开文件用于写，若文件不存在就创建它

    int i = 0;
    string beforeStr = "12345678";

    while (i < 26) {
        if (f1) {
            f1 << beforeStr << str.substr(i, 1) << endl; //使用插入运算符写文件内容
        }
        i = i + 1;
    }
    i = 0;
    while (i < 25) {
        if (f1) {
            f1 << beforeStr << str.substr(i, 2) << endl; //使用插入运算符写文件内容
        }
        i = i + 1;
    }
    beforeStr = "123456789";
    i = 0;
    while (i < 26) {
        if (f1) {
            f1 << beforeStr << str.substr(i, 1) << endl; //使用插入运算符写文件内容
        }
        i = i + 1;
    }
    i = 0;
    while (i < 25) {
        if (f1) {
            f1 << beforeStr << str.substr(i, 2) << endl; //使用插入运算符写文件内容
        }
        i = i + 1;
    }
    beforeStr = "1234567890";
    i = 0;
    while (i < 26) {
        if (f1) {
            f1 << beforeStr << str.substr(i, 1) << endl; //使用插入运算符写文件内容
        }
        i = i + 1;
    }
    i = 0;
    while (i < 25) {
        if (f1) {
            f1 << beforeStr << str.substr(i, 2) << endl; //使用插入运算符写文件内容
        }
        i = i + 1;
    }

    int a;
    int b;
    int c;
    int d;
    int e;
    i = 0;
    int n = 0;
    while (n < 9) {
        n = n + 1;
        i = 0;
        while (i < 22) {
            a = i + 1;
            b = i + 2;
            c = i + 3;
            d = i + 4;
            if (f1) {
                f1 << n << str.substr(i, 1)
                   << n + 1 << str.substr(a, 1)
                   << n + 2 << str.substr(b, 1)
                   << n + 3 << str.substr(c, 1)
                   << endl; //使用插入运算符写文件内容
            }
            i = i + 1;
        }
    }

    i = 0;
    n = 0;
    while (n < 9) {
        n = n + 1;
        while (i < 21) {
            a = i + 1;
            b = i + 2;
            c = i + 3;
            d = i + 4;
            if (f1) {
                f1 << n << str.substr(i, 1)
                   << n + 1 << str.substr(a, 1)
                   << n + 2 << str.substr(b, 1)
                   << n + 3 << str.substr(c, 1)
                   << n + 4 << str.substr(d, 1)
                   << endl; //使用插入运算符写文件内容
            }
            i = i + 1;
        }
    }
    if (f1) {
        f1.close();
    }
}