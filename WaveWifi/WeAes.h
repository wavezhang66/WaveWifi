#ifndef WeAes_H
#define WeAes_H

#include <string>


class WeAes
{
public: WeAes(); 
 std::string EncryptionAES(const std::string& strSrc);
 std::string DecryptionAES(const std::string& strSrc);

	
};
#endif 
