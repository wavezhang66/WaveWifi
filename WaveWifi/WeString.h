/*****************************************************************************
*  Copyright (C) 2019 Wave.Zhang  wavezhang@163.com.                         *
*                                                                            *
*                                                                            *
*  @file     WeString.h                                                       *
*  @brief    对C++字符串处理功能增加                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2019/07/01 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#pragma once
class WeString {
public:
    WeString();
    ~WeString();
    std::string substring(std::string ch, int pos, int length);
    wchar_t* MBCS2Unicode(wchar_t* buff, const char* str);
    char* Unicode2MBCS(char* buff, const wchar_t* str);
    std::wstring str2wstr(std::string str);
    int wputs(const wchar_t* wstr);
    int wputs(std::wstring wstr);

    std::string wstr2str(std::wstring wstr);

};
