// WeMain.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include "WeWifi.h";

using namespace std;

int main()
{
    

    string wifiSsid;
    WeWifi weWifi;
    int start_line_number=0;
    string isLastRun;
	cout << "开始扫描可见Wifi列表...." << "\n";
    cout << "请输入你要扫描的WIFI名称:" ;
    getline(cin, wifiSsid);
    cout << "输入的WIFI名称:" << wifiSsid<<endl;
    cout << "是否继续上次密码行 Y :是，N :否 :";
    getline(cin, isLastRun);
    cout << "您的选择是:" << isLastRun << endl;



	//ofstream f1("E:\\software\\password\\wifinamelog.txt", ios::app); //打开文件用于写，若文件不存在就创建它

	if (_access("logfile", 0) != -1) {
       // cout << "文件夹已存在" << endl;
    } else {
       // cout << "文件夹不存在，创建文件夹" << endl;
        _mkdir("logfile");
    }
    if (_access("config", 0) != -1) {
        //cout << "文件夹已存在" << endl;
    } else {
       // cout << "文件夹不存在，创建文件夹" << endl;
        _mkdir("config");
    }
    if (isLastRun.compare("Y")==0) {
        string s;
        int line_num = 0;
	ifstream infile;
        string file = "./config/" + wifiSsid + "_weconfig.txt";
    infile.open(file.data()); //将文件流对象与文件连接起来
    cout << "infile.is_open():" << infile.is_open() << endl;
    string checkString = "line number:";
    if (infile.is_open()==1)
    {
        while (getline(infile, s)) {
            line_num = line_num + 1;
            int i = s.find(checkString);
            cout << "i:" << i << endl;
            if (i >= 0) {
                cout << "start_line_number:" << s.substr(i + checkString.length()) << endl;
                start_line_number = atoi(s.substr(i + checkString.length()).c_str());
                cout << "将要从:" << start_line_number << "行开始" << endl;
            }

		
        }
    }else
    {
        cout << "没有找到当前WIFI的扫描历史记录，将从第一行开始" << endl;
		start_line_number=0;
    }
		}

    

   

    ofstream f1("./logfile/wifinamelog.txt", ios::app); //打开文件用于写，若文件不存在就创建它
    if (f1) {
         
        f1 << "wifiSsid：" << wifiSsid  << endl; //使用插入运算符写文件内容
      
    } else {
        f1 << "无法访问wifinamelog 文件" << endl; //使用插入运算符写文件内容
    }
    
    if (f1) {
        f1.close();
    }


    weWifi.startDecryptWifi(wifiSsid, start_line_number);

	cout << "按回车退出";
    getline(cin, wifiSsid);
	//oldWif();
	//decryptWifi();

	//genPassword();

	
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件

