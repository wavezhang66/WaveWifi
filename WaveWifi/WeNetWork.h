/*****************************************************************************
*  Copyright (C) 2019 Wave.Zhang  wavezhang@163.com.                         *
*                                                                            *
*                                                                            *
*  @file     WeNetWork.h                                                       *
*  @brief    对网络处理功能增加                                                     *
*  Details.                                                                  *
*                                                                            *
*  @author   Wave.Zhang                                                       *
*  @email    wavezhang@163.com                                              *
*  @version  1.0.0.1(版本号)                                                 *
*  @date     wavezhang@163.com                                              *
*  @license                                 *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  2019/07/01 |  1.0.0.1   | Wave.Zhang      | Create file                     *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#pragma once
#include <winsock.h>

class WeNetWork {
public:
    WeNetWork();
    ~WeNetWork();
    int is_Get_Local_Ip();
};
